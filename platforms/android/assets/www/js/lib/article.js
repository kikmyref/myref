$(document).on("pagebeforeshow","#page3", function(){
	$("#subCategory").html(filter_articles.cur_subCategory_name);
	generateListArticles();
});

$(document).on("pagebeforeshow","#page5", function(){
	//alert(filter_articles.cur_article);
	getOneArticle();
});

function generateListArticles(){
	//filter_articles.data = [1,2,3,4];

	//convert json object to string
	var strFilterArticles = JSON.stringify(filter_articles);

	//convert string to base64 for security purpose
	var b64FilterArticles = $.base64.encode(strFilterArticles);
	//console.log(b64FilterArticles);

	var optAjax = {url: server_url + "getArticles.php",
					type : 'post',
					data : {filter:b64FilterArticles}, // send data name filter to server
					success:function(data){

						//decode date from server to json string, trim whitespace
						var strArticles = $.base64.decode(data.trim());
						//convert json string to object json
						var jArticles = $.parseJSON(strArticles);
						
						if(jArticles.id>0){
							//if success, show data
							//console.log(jArticles.data);
							var articles = jArticles.data;
							var iArticle = articles.length;

							$('#article_list').empty();
							for(var i=0; i < iArticle; i++){
								var article = '<li><a href="#page5" onclick="setArticle('+articles[i].a_id+')">'
								+articles[i].a_document_name+'</a></li>';
								$('#article_list').append(article);
							}
							$('#article_list').listview('refresh');
						}
						else
						{
							//if fail, show mesej from server
							alert(jArticles.msg);
						}
					}
					};

	$.ajax(optAjax);
}

function setArticle(a_id){
	filter_articles.cur_article = a_id;
}

function getOneArticle(){
	//convert json object to string
	var strFilterArticles = JSON.stringify(filter_articles);

	//convert string to base64 for security purpose
	var b64FilterArticles = $.base64.encode(strFilterArticles);
	//console.log(b64FilterArticles);

	var optAjax = {url: server_url + "getOneArticles.php",
					type : 'post',
					data : {filter:b64FilterArticles}, // send data name filter to server
					success:function(data){

						//decode date from server to json string, trim whitespace
						var strArticles = $.base64.decode(data.trim());
						//convert json string to object json
						var jArticles = $.parseJSON(strArticles);
						
						
						if(jArticles.id>0){
							//if success, show data
							console.log(jArticles.data);
							var articles = jArticles.data;
							var iArticle = articles.length;

							$('#document_name,#a_document_name').html(articles[0].a_document_name);
							$('#a_id').html(articles[0].a_id);
							$('#a_category').html(filter_articles.cur_category_name);
							$('#a_type').html(articles[0].a_type);
							$('#a_year').html(articles[0].a_year);
							$('#a_division').html(articles[0].a_division);
							$('#a_abstract').html(articles[0].a_abstract);
						}
						else
						{
							//if fail, show mesej from server
							alert(jArticles.msg);
						}
					}
					};

	$.ajax(optAjax);
}
