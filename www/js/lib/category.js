$(document).on('pagebeforeshow','#page1', function(){
	generateCategory();
});

$(document).on('pagebeforeshow','#page1a', function(){
	generateCategory();
});

$(document).on('pagecreate','#page1a', function(){
    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
});

$(document).on('pagebeforeshow','#page2, #page2a',function(){
	$(".cur_category").html(filter_articles.cur_category_name);

	generateSubCategory();
});

function generateCategory(){
	if(!localStorage.category){
		//process get from database   
		//localStorage.category = JSON.stringify([]);
		getCategoryFromMysql();
	}else{
		//get category from localStorage
		setCategoryFromLocal();
	}
}

function generateSubCategory(){
	if(!localStorage.subcategory){
		//process get from database
		//localStorage.category = JSON.stringify([]);
		getSubCategoryFromMysql();
	}else{
		//get category from localStorage
		setSubCategoryFromLocal();
	}
}

function getCategoryFromMysql(){
	//convert json object to string
	var strFilterArticles = JSON.stringify(filter_articles);

	//convert string to base64 for security purpose
	var b64FilterArticles = $.base64.encode(strFilterArticles);
	//console.log(b64FilterArticles);

	var optAjax = {url: server_url + "getCategory.php",
					type : 'post',
					data : {filter:b64FilterArticles}, // send data name filter to server
					success:function(data){

						//decode date from server to json string, trim whitespace
						var strCategory = $.base64.decode(data.trim());
						//convert json string to object json
						var jCategory = $.parseJSON(strCategory);
						
						if(jCategory.id>0){
							//if success, show data
							//console.log(jCategory.data);
							var aCategory = jCategory.data;
							var iCategory = aCategory.length;

							localStorage.category = JSON.stringify(aCategory);

							setCategoryFromLocal();
						}
						else
						{
							//if fail, show mesej from server
							alert(jCategory.msg); 
						}
					}
					};

	$.ajax(optAjax);
}

function getSubCategoryFromMysql(){
	//convert json object to string
	var strFilterArticles = JSON.stringify(filter_articles);

	//convert string to base64 for security purpose
	var b64FilterArticles = $.base64.encode(strFilterArticles);
	console.log(b64FilterArticles);

	var optAjax = {url: server_url + "getSubCategory.php",
					type : 'post',
					data : {filter:b64FilterArticles}, // send data name filter to server
					success:function(data){

						//decode date from server to json string, trim whitespace
						var strCategory = $.base64.decode(data.trim());
						//convert json string to object json
						var jCategory = $.parseJSON(strCategory);
						
						if(jCategory.id>0){
							//if success, show data
							//console.log(jCategory.data);
							var aCategory = jCategory.data;
							var iCategory = aCategory.length;

							localStorage.subcategory = JSON.stringify(aCategory);

							setSubCategoryFromLocal();
						}
						else
						{
							//if fail, show mesej from server
							alert(jCategory.msg); 
						}
					}
					};

	$.ajax(optAjax);
}

function setCategoryFromLocal(){
	var aCategory = JSON.parse(localStorage['category']);
	var iCat = aCategory.length;

	var panelMenu = $("[data-role='panel'] #nav-mobile");

	console.log(panelMenu);

	panelMenu.empty("");
	panelMenu.append('<br><br><br><li><a href="#page1" data-transition="slide" data-direction="reverse"><i class="mdi-action-home small"></i>Utama</a></li>');


	for(var i = 0; i < iCat; i++){
		var icon = aCategory[i].c_icon;
		var category = '<li><a onclick="setCategory('+aCategory[i].c_id+',\''+aCategory[i].c_name+'\')" class="waves-effect waves-teal" data-transition="slide"><i class="'+icon+' small"></i>'+aCategory[i].c_name+'</a></li>';
		panelMenu.append(category);
	}
	panelMenu.append('<li><a href="#page2a" class="waves-effect waves-teal" data-transition="slide" data-direction="reverse"><i class="mdi-navigation-cancel small"></i>Keluar</a></li>');
}

function setSubCategoryFromLocal(){
	var aCategory = JSON.parse(localStorage['subcategory']);
	var iCat = aCategory.length;
	var cur_category = filter_articles.cur_category;

	var pSubCategory = $(".subcategory_list");
	pSubCategory.empty();
	var j = 0;

	for(var i=0; i < iCat; i++){
		
		var parentId = aCategory[i].c_parent_id;
		var icon = aCategory[i].c_icon;

		if(cur_category==parentId){
			var cssClass = (j%2==0)?'a':'b';
			var subCategory = '<div class="ui-block-'+cssClass+'">'+
			'<a onclick="setSubCategory('+aCategory[i].c_id+',\''+aCategory[i].c_name+'\')">'+
			'<div class="ui-bar ui-bar-a" align="center">'+
			'<i class="'+icon+' large category">'+
			'</i>'+aCategory[i].c_name+'<br/><br/></div></a></div>';
			pSubCategory.append(subCategory);
			j++;
		}
	}
}

function setCategory(c_id, c_name){
	filter_articles.cur_category = c_id;
	filter_articles.cur_category_name = c_name;
	var curPage = $.mobile.activePage.attr('id');
	$.mobile.changePage("#page2");

	if("page2" != curPage)
		$.mobile.changePage("#page2");
	else
		$.mobile.changePage("#page2a");
}

function setSubCategory(c_id, c_name){
	filter_articles.cur_subCategory = c_id;
	filter_articles.cur_subCategory_name = c_name;
	$.mobile.changePage("#page3");
}

function initLoad(){
	generateCategory();
}